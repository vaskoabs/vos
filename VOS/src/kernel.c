
#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef int8_t   s8;
typedef int16_t  s16;
typedef int32_t  s32;


#if defined(__linux__)
#error wrong os
#endif

#if !defined(__i386__)
#error "compile with a ix86-elf compiler"
#endif

typedef enum vga_color {
	VGA_COLOR_BLACK = 0,
	VGA_COLOR_BLUE = 1,
	VGA_COLOR_GREEN = 2,
	VGA_COLOR_CYAN = 3,
	VGA_COLOR_RED = 4,
	VGA_COLOR_MAGENTA = 5,
	VGA_COLOR_BROWN = 6,
	VGA_COLOR_LIGHT_GREY = 7,
	VGA_COLOR_DARK_GREY = 8,
	VGA_COLOR_LIGHT_BLUE = 9,
	VGA_COLOR_LIGHT_GREEN = 10,
	VGA_COLOR_LIGHT_CYAN = 11,
	VGA_COLOR_LIGHT_RED = 12,
	VGA_COLOR_LIGHT_MAGENTA = 13,
	VGA_COLOR_LIGHT_BROWN = 14,
	VGA_COLOR_WHITE = 15,
} vga_color;

static inline u8 vga_entry_color(vga_color foreground, vga_color background)
{
    return foreground|background << 4;
}

static inline uint16_t vga_entry(unsigned char uc, uint8_t color) 
{
	return (uint16_t) uc | (uint16_t) color << 8;
}


static const size_t VGA_WIDTH = 80;
static const size_t VGA_HEIGHT = 25;
size_t terminal_row;
size_t terminal_col;

u8 terminal_color;
u16 *terminal_buffer;


size_t
strlen(const char *string)
{
    int count = 0;
    while(*string++!=0) count++;
    return count;
}

void
init_terminal(void)
{
    terminal_row = 0;
    terminal_col = 0;
    terminal_color = vga_entry_color(VGA_COLOR_WHITE, VGA_COLOR_BLACK);
    terminal_buffer = (u16 *)0xB8000;
    for(size_t y = 0; y < VGA_HEIGHT; ++y)
    {
        for(size_t x = 0; x < VGA_WIDTH; ++x)
        {
            const size_t index = y * VGA_WIDTH + x;
            terminal_buffer[index] = ' ';
        }
    }
}

void
terminal_putchar_at(char c, int x, int y)
{
    terminal_buffer[y * VGA_WIDTH + x] = c;
}

void
terminal_putchar(char c)
{
    terminal_putchar_at(c, terminal_col, terminal_row);
    if(++terminal_col == VGA_WIDTH)
    {
        terminal_col = 0;
    }
    if(++terminal_row == VGA_HEIGHT)
    {
        terminal_row = 0;
    }
}

void
terminal_write_string(const char *string)
{
    int size = strlen(string);
    for(int i = 0; i < size; ++i)
    {
        terminal_putchar(string[i]);
    }
}


void 
kernel_main(void)
{
    init_terminal();
    terminal_write_string("it works!");
}

