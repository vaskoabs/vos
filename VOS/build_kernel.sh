#!/bin/bash


i686-elf-as src/bootloader.asm -o bin/boot.o
i686-elf-gcc -c src/kernel.c -o bin/kernel.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
i686-elf-gcc -T src/linker.ld -o bin/vOS.bin -ffreestanding -O2 -nostdlib bin/boot.o bin/kernel.o -lgcc
