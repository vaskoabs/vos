#!/bin/bash

mkdir -p isodir/boot/grub
cp bin/vOS.bin isodir/boot/vOS.bin
cp grub.cfg isodir/boot/grub/grub.cfg
grub-mkrescue -o vOS.iso isodir

